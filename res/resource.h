#include <cpuid.h>

#define APP_VERSION								0,1,0,0
#define APP_VERSION_STR							"0.1.0.0"

#if defined(BUILD_AVX)
    #define RES_FILEDESC    "Image to XLSX x86_64_AVX"
#elif defined(BUILD_SSE3)
    #define RES_FILEDESC    "Image to XLSX x86_64_SSE3"
#else
    #define RES_FILEDESC    "Image to XLSX x86_64"
#endif // __AVX__


#define IDC_ICON_A                              101

