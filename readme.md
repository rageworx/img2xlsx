# img2xlsx

## Require to build source.
1. Latest version of MinGW-W64 or Mac OS X ( El capitan ) with llvm-gcc
1. Lastest version of fl_imgtk (debug & sse3(openmp option) )
1. Latest version of fltk-1.3.4-2-ts-ext5 or higher.

## Updates in latest version 
* Version 0.1 
    1. First empty projects.
	   
## Previous updates
* Version 0.0
    1. None.

## Supported OS
1. Windows NT 6.1 or above
1. Mac OS X El Capitan or above
1. Linux (May included Makefile.linux)
