#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Menu_.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Menu_Button.H>

#include "Fl_ImageViewer.H"
#include "Fl_Image_Button.h"

#include <pthread.h>

#include <vector>
#include <string>

class WMain : public Fl_ImageViewerNotifier
{
    typedef struct
        _ThreadParam{
            pthread_t   ptt;
            long        paramL;
            void*       paramData;
        }ThreadParam;

    public:
        WMain( int argc, char** argv );
        ~WMain();

    public:
        int Run();

    public:
        void TimerCall();
        static void FlTimerCB( void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->TimerCall();
            }
        }
        void* PThreadCall( ThreadParam* tparam );
        static void* PThreadCB( void* p )
        {
            if ( p != NULL )
            {
                ThreadParam* tp = (ThreadParam*)p;
                WMain* pwm = (WMain*)tp->paramData;
                return pwm->PThreadCall(tp);
            }
            return NULL;
        }
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }

    private:
        void setdefaultwintitle();
        void createComponents();
        void registerPopMenu();
        void getEnvironments();
        void updateImageHistogram();
        void updateCrossFadeBG();
        // int  testImageFile();
        // result : <0 = unknown, 1=JPG, 2=PNG, 3=BMP ...
#ifdef SUPPORT_WCHAR
        int  testImageFile( const wchar_t* imgfp = NULL, char** buff = NULL, size_t* buffsz = NULL );
#else
        int  testImageFile( const char* imgfp = NULL, char** buff = NULL, size_t* buffsz = NULL );
#endif
        void loadImage();
        int  sensFitting( const Fl_RGB_Image* img );
        void saveImageProc();
        void rotateLeft();
        void rotateRight();
        bool createThread( unsigned idx );
        void killThread( unsigned idx );

    private:
        int     _argc;
        char**  _argv;

    private:
        float       crossfade_ratio;
        unsigned    crossfade_idx;
        bool        timermutex;
        bool        busyflag;

    protected: /// inherit to Fl_ImageViewerNotifier
        void OnMouseMove( void* w, int x, int y, bool inside );
        void OnMouseClick( void* w, int x, int y, unsigned btn );
        void OnKeyPressed( void* w, unsigned short k, int s, int c, int a );
        void OnDropFiles( void* w, const char* files );
        void OnDrawCompleted();

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpControl;
        Fl_Group*           grpControlActive;
        Fl_Group*           grpControlEmpty;
        Fl_Group*           grpControlTool;
        Fl_Box*             boxControl;
        Fl_Group*           grpImageView;
        Fl_ImageViewer*     boxImgPreView;
        Fl_RGB_Image*       imgControlBG;
        Fl_RGB_Image*       imgSource;
        Fl_RGB_Image*       imgResized;

        Fl_Choice*          chsRsFilter;
        Fl_Image_Button*    btnLoad;
        Fl_Image_Button*    btnRotLeft;
        Fl_Image_Button*    btnRotRight;
        Fl_Image_Button*    btnApply;
        Fl_Image_Button*    btnSaveTo;

        Fl_Menu_Button*     popMenu;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:
#ifdef SUPPORT_WCHAR
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;
#else
        std::string         pathHome;
        std::string         pathSystemBase;
        std::string         pathUserData;
        std::string         pathUserRoaming;
#endif

    protected:
#ifdef SUPPORT_WCHAR
        std::wstring        pathImage;
        std::wstring        imgFName;
#else
        std::string         pathImage;
        std::string         imgFName;
#endif
        std::string         imgFNameUTF8;

    protected:
        #define             MAX_THREADS_CNT             3
        #define             THREAD_KILL_ALL             -1
        #define             THREAD_JOB_RESIZE_IMAGE     0
        #define             THREAD_JOB_UPDATE_HISTO     1
        #define             THREAD_JOB_SAVE_FILE        2
        ThreadParam*        threads[MAX_THREADS_CNT];

};

#endif // __WINMAIN_H__
