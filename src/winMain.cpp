#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#ifdef __linux__
    #include <signal.h>
#endif

#include "winMain.h"
#include "fl_imgtk.h"
#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#include <FL/images/png.h>

#include "resource.h"
#include "dtools.h"
#include "ltools.h"

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "img2xlsx"
#define DEF_WIDGET_FSZ          12
#ifdef __linux__
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif // __linux__

#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xFFFFFF00

////////////////////////////////////////////////////////////////////////////////

string resourcebase;

bool getResource( const char* scheme, uchar** buff, unsigned* buffsz )
{
#ifdef _WIN32
    wchar_t convscheme[80] = {0,};

    fl_utf8towc( scheme, strlen(scheme), convscheme, 80 );

    HRSRC rsrc = FindResource( NULL, convscheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    uchar* cpbuff = new uchar[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }
#elif defined(__APPLE__)
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    rescomb += ".png";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }
#ifdef DEBUG
    printf("Failure.\n");
#endif

#else /// --> Linux
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    rescomb += ".png";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }

#endif /// of _WIN32

    return false;
}

Fl_RGB_Image* createResImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        unsigned buffsz = 0;

        if ( getResource( scheme, &buff, &buffsz ) == true )
        {
            Fl_RGB_Image* retimg = new Fl_PNG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

#ifdef _WIN32
typedef int(__stdcall *MSGBOXWAPI)(IN HWND, IN LPCWSTR, IN LPCWSTR, IN UINT, IN WORD, IN DWORD);

int X_MessageBoxTimeout(HWND hWnd, const TCHAR* sText, const TCHAR* sCaption, UINT uType, DWORD dwMilliseconds)
{
    int retI = 0;

    HMODULE hUser32 = LoadLibraryA( "user32.dll" );

    if ( hUser32 != NULL )
    {
#ifdef UNICODE
        static \
        MSGBOXWAPI MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutW");
        retI = MessageBoxTimeoutW(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
#else
        static \
        MSGBOXWAPI MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutA");
        retI = MessageBoxTimeoutA(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
#endif
        FreeLibrary( hUser32 );
    }
    else
    {
        retI = MessageBox(hWnd, sText, sCaption, uType);
    }

    return retI;
}
#endif // _WIN32

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv )
 :  _argc( argc ),
    _argv( argv ),
    crossfade_idx( 0 ),
    crossfade_ratio( 0.0f ),
    timermutex(false),
    window( NULL ),
    imgControlBG( NULL ),
    imgSource( NULL ),
    imgResized( NULL ),
    busyflag( false )
{
    memset( threads, 0, sizeof( pthread_t ) * MAX_THREADS_CNT );

    getEnvironments();
    createComponents();
    registerPopMenu();

#ifdef _WIN32
    HICON \
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    HICON \
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    if ( window != NULL )
    {
        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
    }
#endif /// of _WIN32

    // registering timer.
    Fl::add_timeout( 0.01f, WMain::FlTimerCB, this );
}

WMain::~WMain()
{
    killThread( THREAD_KILL_ALL );

    Fl::remove_timeout( WMain::FlTimerCB );
}

int WMain::Run()
{
    if ( window != NULL )
    {
        return Fl::run();
    }

    return 0;
}

void WMain::TimerCall()
{
    if ( timermutex == true )
    {
#ifdef DEBUG
        printf("Ignored Timer\n");
#endif // DEBUG
        return;
    }

    timermutex = true;

#ifdef __APPLE__
    // repeating timer helps redrawing window of FLTK.
    Fl::repeat_timeout( 0.3f, WMain::FlTimerCB, this );
#else
    window->redraw();
    Fl::wait();
#endif /// of __APPLE__
    timermutex = false;

#ifdef DEBUG
    printf("Timer call processed.\n");
#endif // DEBUG
}

void* WMain::PThreadCall( ThreadParam* tparam )
{
    if ( tparam == NULL )
        return NULL;

    long pLL = tparam->paramL;

    switch ( pLL )
    {
        case THREAD_JOB_RESIZE_IMAGE:
            {
                window->cursor( FL_CURSOR_WAIT );

                int curftype = chsRsFilter->value();

                grpControl->deactivate();

                fl_imgtk::discard_user_rgb_image( imgResized );

                float paramf[4] = {0.0f, };

                for( unsigned cnt=0; cnt<4; cnt++ )
                {
                    const char* convs2f = inpParam[ cnt ]->value();

                    if ( convs2f != NULL )
                    {
                        paramf[ cnt ] = atof( convs2f );
                    }
                }

                switch( curftype )
                {
                    case 0: /// near-box
                        break;

                    case 1: /// bilinear
                        break;

                    case 2: /// bicubic
                        break;

                    case 3: /// lanzcos-3
                        break;
                }

                bool bContinue2Histo = false;

                if ( imgResized != NULL )
                {
                    boxImgPreView->image( imgResized, sensFitting( imgResized ) );
                    bContinue2Histo = true;
                    btnCopy2Clip->show();
                    btnSaveTo->show();
                }
                else
                {
                    btnCopy2Clip->hide();
                    btnSaveTo->hide();
                }

                window->cursor( FL_CURSOR_DEFAULT );

                grpControl->activate();
                btnRotLeft->activate();
                btnRotRight->activate();
                btnCopy2Clip->activate();
                btnApply->activate();
                grpControl->damage(0);

                if ( bContinue2Histo == false )
                {
                    Fl::awake();
                    break;
                }
            }
            // do not break here,
            // just goes down for update histogram.

        case THREAD_JOB_UPDATE_HISTO:
            {
                // check for cached image exists.
                if ( boxImgPreView->resizedimage() != NULL )
                {
                    boxImgPreView->updateHistogram();
                    boxImgPreView->damage( 0 );

                    Fl::awake();
                }
            }
            break;

        case THREAD_JOB_SAVE_FILE:
            {
                saveImageProc();
            }
            break;
    }

    boxImgPreView->take_focus();

    // Make window drawing called back from system.
    // Warning : Mac OS X can not calls timer in thread.
#ifdef __APPLE__
    window->redraw();
#else
    Fl::repeat_timeout( 0.05f, WMain::FlTimerCB, this );
#endif // of __APPLE__

    delete threads[ pLL ];
    threads[ pLL ] = NULL;

    pthread_exit( NULL );

    return NULL;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        return;
    }

    if ( w == chsRsFilter )
    {
        int curidx = chsRsFilter->value();

        boxControl->redraw();

        return;
    }

    if ( w == btnLoad )
    {
        loadImage();
    }

    if ( w == btnApply )
    {
        btnApply->deactivate();
        btnRotLeft->deactivate();
        btnRotRight->deactivate();

        killThread( -1 );
        createThread( THREAD_JOB_RESIZE_IMAGE );

        return;
    }

    if ( w == btnSaveTo )
    {
        if ( imgResized != NULL )
        {
            btnCopy2Clip->deactivate();
            btnSaveTo->deactivate();
#ifndef __APPLE__
            createThread( THREAD_JOB_SAVE_FILE );
#else
            saveImageProc();
#endif /// of __APPLE__
        }

        return;
    }

    if ( w == btnRotLeft )
    {
        grpControl->deactivate();
        rotateLeft();
        grpControl->activate();

        return;
    }

    if ( w == btnRotRight )
    {
        grpControl->deactivate();
        rotateRight();
        grpControl->activate();

        return;
    }

    if ( w == popMenu )
    {
        int pmsel = popMenu->value();
        switch( pmsel )
        {
            case 0: /// Rotate Left
                grpControl->deactivate();
                rotateLeft();
                grpControl->activate();
                break;

            case 1: /// Rotate Right
                grpControl->deactivate();
                rotateRight();
                grpControl->activate();
                break;

            case 2: /// Hide top menu
                if ( grpControl->visible_r() > 0 )
                {
                    grpControl->hide();
                }
                else
                {
                    grpControl->show();
                }
                break;
        }

        return;
    }
}

void WMain::OnMouseMove( void* w, int x, int y, bool inside )
{

}

void WMain::OnMouseClick( void* w, int x, int y, unsigned btn )
{
    if ( btn == FL_BUTTON3 )
    {
        if ( popMenu != NULL )
        {
            popMenu->position( x, y );
            popMenu->show();
        }
    }
}

void WMain::OnKeyPressed( void* w, unsigned short k, int s, int c, int a )
{
#ifdef DEBUG
    printf("OnKeyPressed!\n");
#endif // DEBUG
}

void WMain::OnDropFiles( void* w, const char* files )
{
    if ( busyflag == true )
        return;

    killThread( THREAD_KILL_ALL );

    #ifdef DEBUG
    printf("OnDropFiles : %s\n", files);
    #endif // DEBUG

    if ( ( w == boxImgPreView ) && ( files != NULL ) )
    {
        // decode UTF-8 strings to wide characters.

        size_t decodelen = strlen( files );
        if ( decodelen > 0 )
        {
#ifdef SUPPORT_WCHAR
            wchar_t* decodedstr = new wchar_t[ decodelen + 1 ];
            if ( decodedstr != NULL )
            {
                fl_utf8towc( files, strlen( files ),
                             decodedstr, decodelen + 1 );

                wstring tmpfiles = decodedstr;

                delete[] decodedstr;

                vector<wstring> tmpfilelist;

                tmpfilelist = split( tmpfiles, L'\n' );
#else
           const char* decodedstr = files;
           if ( decodedstr != NULL )
           {
                vector<string> tmpfilelist;
                string tmpfiles = decodedstr;

                tmpfilelist = split( tmpfiles, '\n' );
#endif
                if ( tmpfilelist.size() > 0 )
                {
                    #ifdef __linux__
                    for( size_t cnt=0; cnt<tmpfilelist.size(); cnt++ )
                    {
                        size_t fpos = tmpfilelist[cnt].find("file:/");
                        if ( fpos != string::npos )
                        {
                            tmpfilelist[cnt].erase( fpos, 6 );
                        }
                    }
                    #endif // __linux__


                    for( size_t cnt=0; cnt<tmpfilelist.size(); cnt++ )
                    {
                        // check while image loads.
                        char* tmpbuff = NULL;
                        size_t tmpbuffsz = 0;

                        int ret = testImageFile( tmpfilelist[ cnt ].c_str(), &tmpbuff, &tmpbuffsz );
                        if ( ( ret > 0 ) && ( tmpbuffsz > 0 ) )
                        {
                            Fl_RGB_Image* tmpimg = NULL;

                            switch( ret )
                            {
                                case 1: /// JPEG
                                    tmpimg = new Fl_JPEG_Image( "JPGIMG", (const uchar*)tmpbuff );
                                    break;

                                case 2: /// PNG
                                    tmpimg = new Fl_PNG_Image( "PNGIMAGE", (const uchar*)tmpbuff, tmpbuffsz );
                                    break;

                                case 3: /// BMP
                                    tmpimg = fl_imgtk::createBMPmemory( (const char*)tmpbuff, tmpbuffsz );
                                    break;
                            }

                            delete[] tmpbuff;

                            if ( tmpimg != NULL )
                            {
                                fl_imgtk::discard_user_rgb_image( imgSource );

                                imgSource = tmpimg;

                                boxImgPreView->image( imgSource, sensFitting( imgSource ) );
                                boxImgPreView->redraw();

                                pathImage = stripFilePath( tmpfilelist[ cnt ].c_str() );
                                imgFName  = stripFileName( tmpfilelist[ cnt ].c_str() );

                                // Convert source image to new name to be saved.
#ifdef SUPPORT_WCHAR
                                wstring tmpwstr = imgFName;
                                size_t fpos = tmpwstr.find_last_of( L'.' );
                                if ( fpos != string::npos )
                                {
                                    tmpwstr = tmpwstr.substr( 0, fpos );
                                }
                                char tmputf8[1024] = {0,};
                                fl_utf8fromwc( tmputf8, 1024, tmpwstr.c_str(), tmpwstr.size() );
                                imgFNameUTF8 = tmputf8;
#else
                                string tmpstr = imgFName;
                                size_t fpos = tmpstr.find_last_of( '.' );
                                if ( fpos != string::npos )
                                {
                                    tmpstr = tmpstr.substr( 0, fpos );
                                }
                                imgFNameUTF8 = tmpstr;
#endif
                                // Update window title.
                                setdefaultwintitle();
                                string tmpwintitle = wintitlestr;
                                sprintf( wintitlestr, "%s : %s", imgFNameUTF8.c_str(), tmpwintitle.c_str() );
                                window->label( wintitlestr );

                                updateImageHistogram();

                                fl_imgtk::discard_user_rgb_image( imgResized );

                                btnApply->show();
                                btnRotLeft->show();
                                btnRotRight->show();

                                btnCopy2Clip->hide();
                                btnSaveTo->hide();

                                boxControl->redraw();
                                return;
                            }
                        }

                    }
                }
            }
        }

    }
}

void WMain::OnDrawCompleted()
{
    boxControl->hide();
    boxControl->deimage();
    //boxControl->image( NULL );

    if ( imgControlBG != NULL )
    {
        fl_imgtk::discard_user_rgb_image( imgControlBG );
        //delete imgControlBG;
    }

    uchar* widgetbuff = new uchar[ boxControl->w() * boxControl->h() * 3 + 1];
    if ( widgetbuff != NULL )
    {
        // Read current window pixels to buffer for create a new Fl_RGB_Image.
        fl_read_image( widgetbuff, boxControl->x(), boxControl->y(),
                                   boxControl->w(), boxControl->h() );

        Fl_RGB_Image* imgcntl = new Fl_RGB_Image( widgetbuff,
                                                  boxControl->w(),
                                                  boxControl->h(),
                                                  3 );

        if ( imgcntl != NULL )
        {
            fl_imgtk::brightness_ex( imgcntl, -50 );
            fl_imgtk::draw_line( imgcntl,
                                 0, 0,
                                 boxControl->w(), 0,
                                 0xFFFFFFFF );
            fl_imgtk::draw_line( imgcntl,
                                 0, 2,
                                 boxControl->w(), 2,
                                 0xFFFFFFFF );
            fl_imgtk::draw_line( imgcntl,
                                 0, boxControl->h()-1,
                                 boxControl->w(), boxControl->h()-1,
                                 0xFFFFFFFF );
            fl_imgtk::blurredimage_ex( imgcntl, 8 );
            fl_imgtk::draw_line( imgcntl,
                                 0, boxControl->h()-1,
                                 boxControl->w(), boxControl->h()-1,
                                 0xFFFFFF1F );

            imgControlBG = imgcntl;

            boxControl->image( imgControlBG );
        }
        else
        {
            delete[] widgetbuff;
        }
    }

    boxControl->show();
}

void WMain::getEnvironments()
{
#ifdef _WIN32
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );
#ifdef DEBUG
    wprintf( L"pathHome        : %S\n", pathHome.c_str() );
    wprintf( L"pathSystemBase  : %S\n", pathSystemBase.c_str() );
    wprintf( L"pathUserData    : %S\n", pathUserData.c_str() );
    wprintf( L"pathUserRoaming : %S\n", pathUserRoaming.c_str() );
#endif // DEBUG
#elif defined(__APPLE__)
    resourcebase = _argv[0];
    size_t fpos = resourcebase.find_last_of('/');
    if ( fpos != string::npos )
    {
        resourcebase = resourcebase.substr( 0, fpos );

        fpos = resourcebase.find_last_of('/');

        if ( fpos != string::npos )
        {
            resourcebase = resourcebase.substr( 0, fpos );
            resourcebase += "/Resources";
        }
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;
#else /// must be Linux
    resourcebase = _argv[0];
    size_t fpos = resourcebase.find_last_of('/');
    if ( fpos != string::npos )
    {
        resourcebase = resourcebase.substr( 0, fpos );
        resourcebase += "/res";
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;

	#ifdef DEBUG
	printf("#DEBUG.PATHES\n");
	printf("HOME: %s\n", pathHome.c_str());
	printf("resourcebase: %s\n",resourcebase.c_str());
	#endif
#endif /// of _WIN32, __APPLE__ and __linux___
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    sprintf( wintitlestr, "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::createComponents()
{
    setdefaultwintitle();

    window = new Fl_Double_Window( 790, 440, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        // continue to child components ...
        window->begin();

            boxImgPreView = new Fl_ImageViewer( 0, 0, window->w(), window->h() );
            if ( boxImgPreView != NULL )
            {
                boxImgPreView->notifier( this );
                boxImgPreView->box( FL_NO_BOX );
            }

            grpControl = new Fl_Group( 0, 0, window->w(), 50 );

            if ( grpControl != NULL )
            {
                grpControl->begin();

                boxControl = new Fl_Box( grpControl->x(), grpControl->y(),
                                         grpControl->w(), grpControl->h() );
                if ( boxControl != NULL )
                {
                    boxControl->box( FL_NO_BOX );
                }

                int gc_a_w = 530;

                grpControlActive = new Fl_Group( grpControl->x(), grpControl->y(),
                                                 gc_a_w, grpControl->h() );

                if ( grpControlActive != NULL )
                {
                    grpControlActive->box( FL_NO_BOX );
                    grpControlActive->begin();
                }

                int l_x = 50;
                int l_y = 12;
                int l_w = 120;
                int l_h = 25;

                chsRsFilter = new Fl_Choice( l_x, l_y, l_w, l_h );
                if ( chsRsFilter != NULL )
                {
                    chsRsFilter->box( FL_NO_BOX );
                    chsRsFilter->color( window->color() );
                    chsRsFilter->labelcolor( window->labelcolor() );
                    chsRsFilter->textcolor( 0xFF663300 );
                    chsRsFilter->textsize( window->labelsize() + 2 );
                    chsRsFilter->labelfont( window->labelfont() );
                    chsRsFilter->labelsize( window->labelsize() );
                    chsRsFilter->when( FL_WHEN_CHANGED );
                    chsRsFilter->clear_visible_focus();

                    chsRsFilter->image( createResImage( "simg_eyefilter" ) );
                    chsRsFilter->tooltip( "Resize types" );

                    chsRsFilter->add( "None" );
                    chsRsFilter->add( "Bi-Linear" );
                    chsRsFilter->add( "Bi-Cubic" );
                    chsRsFilter->add( "LANCZOS-3");

                    chsRsFilter->value( 0 );
                    chsRsFilter->callback( WMain::WidgetCB, this );
                }

                l_y = 12;
                l_h = 25;
                l_x += l_w + 50;
                l_w = 50;

                if ( grpControlActive != NULL )
                {
                    grpControlActive->end();
                }


                grpControlEmpty = new Fl_Group( gc_a_w, grpControl->y(), 10, grpControl->h() );
                if( grpControlEmpty != NULL )
                {
                    grpControlEmpty->begin();

                    Fl_Box* boxSep = new Fl_Box( grpControlEmpty->x(),
                                                 grpControlEmpty->y(),
                                                 grpControlEmpty->w(),
                                                 grpControlEmpty->h(),
                                                 "@2line" );
                    if ( boxSep != NULL )
                    {
                        boxSep->box( FL_NO_BOX );
                        boxSep->labelcolor( 0x77777700 );
                    }
                    grpControlEmpty->end();
                }

                l_x = gc_a_w + 10;

                grpControlTool = new Fl_Group( l_x, grpControl->y(),
                                               grpControl->w() - l_x, grpControl->h() );
                if ( grpControlTool != NULL )
                {
                    grpControlTool->begin();
                }

                l_y = 10;
                l_h = 30;
                l_w = 30;

                btnLoad = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnLoad != NULL )
                {
                    btnLoad->image( createResImage( "simg_load" ) );
                    btnLoad->tooltip( "Load image" );
                    btnLoad->callback( WMain::WidgetCB, this );
                }

                l_x += l_w + 5;

                btnRotLeft = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnRotLeft != NULL )
                {
                    btnRotLeft->image( createResImage( "simg_rot_l45d" ) );
                    btnRotLeft->tooltip( "Rotate image to 90 degree in clockwise" );
                    btnRotLeft->callback( WMain::WidgetCB, this );
                    btnRotLeft->hide();
                }

                l_x += l_w + 5;

                btnRotRight = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnRotRight != NULL )
                {
                    btnRotRight->image( createResImage( "simg_rot_r45d" ) );
                    btnRotRight->tooltip( "Rotate image to 90 degree in counterclockwise" );
                    btnRotRight->callback( WMain::WidgetCB, this );
                    btnRotRight->hide();
                }

                l_x += l_w + 5;

                btnApply = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnApply != NULL )
                {
                    btnApply->image( createResImage( "simg_apply" ) );
                    btnApply->tooltip( "Apply changes" );
                    btnApply->callback( WMain::WidgetCB, this );
                    btnApply->hide();
                }

                l_x += l_w + 5;

                btnSaveTo = new Fl_Image_Button( l_x, l_y, l_w, l_h );
                if ( btnSaveTo != NULL )
                {
                    btnSaveTo->box( FL_NO_BOX );
                    btnSaveTo->color( 0xFFFFFF00 );
                    btnSaveTo->labelcolor( window->labelcolor() );
                    btnSaveTo->labelfont( window->labelfont() );
                    btnSaveTo->labelsize( window->labelsize() );
                    btnSaveTo->clear_visible_focus();

                    btnSaveTo->image( createResImage( "simg_save" ) );
                    btnSaveTo->tooltip( "Save image to a XLSX ..." );

                    btnSaveTo->callback( WMain::WidgetCB, this );

                    btnSaveTo->hide();
                }

                if ( grpControlTool != NULL )
                {
                    grpControlTool->end();
                }

                grpControl->end();
                grpControl->resizable( grpControlEmpty );
            }


            grpImageView = new Fl_Group( 0, 50, window->w(), window->h() - 50 );

            if ( grpImageView != NULL )
            {
                grpImageView->begin();

                // Something here ...

                grpImageView->end();
                grpImageView->hide();
            }

        window->end();

        if ( grpImageView != NULL )
        {
            window->resizable( grpImageView );
            window->size_range( window->w(), window->h() );
        }

        if ( chsRsFilter != NULL )
        {
            chsRsFilter->do_callback();
        }

        window->show();
    }
}

void WMain::registerPopMenu()
{
    popMenu = new Fl_Menu_Button( 0, 0, window->w(), window->h() );
    if ( popMenu != NULL )
    {
        popMenu->type( Fl_Menu_Button::POPUP3 );
        popMenu->color( window->color(), window->labelcolor() );
        popMenu->selection_color( window->color() );
        popMenu->labelfont( window->labelfont() );
        popMenu->textfont( window->labelfont() );
        popMenu->labelsize( window->labelsize() );
        popMenu->textsize( window->labelsize() );
        popMenu->labelcolor( window->labelcolor() );
        popMenu->textcolor( window->labelcolor() );

#ifdef __APPLE__
        int ckey[] = {
            FL_COMMAND + FL_Left,
            FL_COMMAND + FL_Right,
            0x20 };
#else
        int ckey[] = {
            FL_CTRL + FL_Left,
            FL_CTRL + FL_Right,
            0x20 };
#endif /// of __APPLE__

        popMenu->add( "Rotate Left       \t", ckey[1], WMain::WidgetCB, this, 0 );
        popMenu->add( "_Rotate Right     \t", ckey[2], WMain::WidgetCB, this, 0 );
        popMenu->add( "Show&&Hide top menu\t", ckey[3], WMain::WidgetCB, this, 0 );

        // Ok. then, insert popup to main window.
        window->add( popMenu );
    }
}

void WMain::updateImageHistogram()
{
    createThread( THREAD_JOB_UPDATE_HISTO );
}

void WMain::updateCrossFadeBG()
{
    // Check current cross-fading condition ...

}

#ifdef SUPPORT_WCHAR
int WMain::testImageFile( const wchar_t* imgfp, char** buff,size_t* buffsz )
{
    int reti = -1;

    if ( imgfp != NULL )
    {
        FILE* fp = _wfopen( imgfp, L"rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            size_t flen = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            if ( flen > 32 )
            {
                // Test
                char testbuff[32] = {0,};

                fread( testbuff, 1, 32, fp );
                fseek( fp, 0, SEEK_SET );

                const uchar jpghdr[3] = { 0xFF, 0xD8, 0xFF };

                // is JPEG ???
                if( strncmp( &testbuff[0], (const char*)jpghdr, 3 ) == 0 )
                {
                    reti = 1; /// JPEG.
                }
                else
                if( strncmp( &testbuff[1], "PNG", 3 ) == 0 )
                {
                    reti = 2; /// PNG.
                }
                else
                if( strncmp( &testbuff[0], "BM", 2 ) == 0 )
                {
                    reti = 3; /// BMP.
                }

                if ( reti > 0 )
                {
                    *buff = new char[ flen ];
                    if ( *buff != NULL )
                    {
                        fread( *buff, 1, flen, fp );

                        if( buffsz != NULL )
                        {
                            *buffsz = flen;
                        }
                    }
                }
            }

            fclose( fp );
        }
    }

    return reti;
}
#else
int WMain::testImageFile( const char* imgfp, char** buff,size_t* buffsz )
{
    int reti = -1;

    if ( imgfp != NULL )
    {
        FILE* fp = fopen( imgfp, "rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            size_t flen = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            if ( flen > 32 )
            {
                // Test
                char testbuff[32] = {0,};

                fread( testbuff, 1, 32, fp );
                fseek( fp, 0, SEEK_SET );

                const uchar jpghdr[3] = { 0xFF, 0xD8, 0xFF };

                // is JPEG ???
                if( strncmp( &testbuff[0], (const char*)jpghdr, 3 ) == 0 )
                {
                    reti = 1; /// JPEG.
                }
                else
                if( strncmp( &testbuff[1], "PNG", 3 ) == 0 )
                {
                    reti = 2; /// PNG.
                }
                else
                if( strncmp( &testbuff[0], "BM", 2 ) == 0 )
                {
                    reti = 3; /// BMP.
                }

                if ( reti > 0 )
                {
                    *buff = new char[ flen ];
                    if ( *buff != NULL )
                    {
                        fread( *buff, 1, flen, fp );

                        if( buffsz != NULL )
                        {
                            *buffsz = flen;
                        }
                    }
                }
            }

            fclose( fp );
        }
    }

    return reti;
}
#endif

void WMain::loadImage()
{
    Fl_Native_File_Chooser nFC;

    nFC.title( "Select image file to load." );
    nFC.type( Fl_Native_File_Chooser::BROWSE_FILE );
    nFC.options( Fl_Native_File_Chooser::PREVIEW );
#if defined(__APPLE__) || defined(__linux__)
    nFC.filter( "Supported Image\t*.{png,jpg,jpeg,bmp}\n"
                "PNG Image\t*.png\n"
                "JPEG Image\t*.{jpg,jpeg}\n"
                "Windows BMP\t*.bmp" );
#else
    nFC.filter( "Supported Image\t*.png;*.jpg;*.jpeg;*.bmp\n"
                "PNG Image\t*.png;*.PNG\n"
                "JPEG Image\t*.jpg;*.JPG;*.JPEG;*.jpeg\n"
                "Windows BMP\t*.bmp;*.BMP" );
#endif /// of __APPLE__
    nFC.preset_file( NULL );

    char refconvpath[1024] = {0,};

#ifdef SUPPORT_WCHAR
    if ( pathImage.size() > 0 )
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathImage.c_str(), pathImage.size() );
    }
    else
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathHome.c_str(), pathHome.size() );
    }
#else
    strcpy( refconvpath, pathHome.c_str() );
#endif
    nFC.directory( refconvpath );

    int retVal = nFC.show();

    if ( retVal == 0 )
    {
        string loadfn = nFC.filename(0);

        OnDropFiles( boxImgPreView, loadfn.c_str() );
    }
}

int WMain::sensFitting( const Fl_RGB_Image* img )
{
    // Makes current image to where fit
    if ( img != NULL )
    {
        if ( img->w() > img->h() )
        {
            boxImgPreView->fitwidth();
            return 0;
        }
        else
        if ( img->w() < img->h() )
        {
            boxImgPreView->fitheight();
            return 1;
        }
    }

    boxImgPreView->fitwidth();
    return 0;
}

void WMain::saveImageProc()
{
    Fl_Native_File_Chooser nFC;

    static string presetfn;

    presetfn = imgFNameUTF8;
    presetfn += "_hdr.xlsx";

    nFC.title( "Select XLSX file to save." );
    nFC.type( Fl_Native_File_Chooser::BROWSE_SAVE_FILE );
    nFC.options( Fl_Native_File_Chooser::USE_FILTER_EXT
                 | Fl_Native_File_Chooser::SAVEAS_CONFIRM );
    nFC.filter( "Excel X file\t*.xlsx" );
    nFC.preset_file( presetfn.c_str() );

    char refconvpath[1024] = {0,};

    busyflag = true;

#ifdef _WIN32
    if ( pathImage.size() > 0 )
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathImage.c_str(), pathImage.size() );
    }
    else
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathHome.c_str(), pathHome.size() );
    }
#else
    strcpy( refconvpath, imgFNameUTF8.c_str() );
#endif

    nFC.directory( refconvpath );

    int retVal = nFC.show();

    if ( retVal == 0 )
    {
#ifdef __APPLE__
        Fl::check();
#endif /// of __APPLE__

        imgFNameUTF8 = nFC.directory();

        string savefn = nFC.filename(0);
        string testfn = removeFilePath( savefn.c_str() );

        if ( testfn.find_last_of('.') == string::npos )
        {
            savefn += ".xlsx";
        }

        window->cursor( FL_CURSOR_WAIT );

        //save2png( imgResized, savefn.c_str() );

        window->cursor( FL_CURSOR_DEFAULT );

        Fl::awake();
    }

    btnCopy2Clip->activate();
    btnSaveTo->activate();

    busyflag = false;
}

void WMain::rotateRight()
{
    if ( imgSource != NULL )
    {
        Fl_RGB_Image* rotImg = fl_imgtk::rotate90( imgSource );
        if ( rotImg != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgSource );
            imgSource = rotImg;
        }
    }

    if ( imgResized != NULL )
    {
        boxImgPreView->image( NULL );
        Fl_RGB_Image* newHDRed = fl_imgtk::rotate90( imgResized );
        if ( newHDRed != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgResized );
            imgResized = newHDRed;

            boxImgPreView->image( imgResized,sensFitting( imgResized ) );
            boxImgPreView->redraw();
        }
    }
    else
    {
        boxImgPreView->image( imgSource, sensFitting( imgSource ) );
        boxImgPreView->redraw();
    }

    updateImageHistogram();
}

void WMain::rotateLeft()
{
    if ( imgSource != NULL )
    {
        Fl_RGB_Image* rotImg = fl_imgtk::rotate270( imgSource );
        if ( rotImg != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgSource );
            imgSource = rotImg;
        }
    }

    if ( imgResized != NULL )
    {
        boxImgPreView->image( NULL );
        Fl_RGB_Image* newHDRed = fl_imgtk::rotate270( imgResized );
        if ( newHDRed != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgResized );
            imgResized = newHDRed;

            boxImgPreView->image( imgResized, sensFitting( imgResized ) );
            boxImgPreView->redraw();
        }
    }
    else
    {
        boxImgPreView->image( imgSource, sensFitting( imgSource ) );
        boxImgPreView->redraw();
    }

    updateImageHistogram();
}

bool WMain::createThread( unsigned idx )
{
    if ( ( idx >= 0 ) && ( idx < MAX_THREADS_CNT ) )
    {
        killThread( idx );

        threads[idx] = new ThreadParam;
        if ( threads[idx] != NULL )
        {
            threads[idx]->paramL = idx;
            threads[idx]->paramData = this;

            pthread_create( &threads[idx]->ptt,
                            NULL, WMain::PThreadCB,
                            threads[idx] );

            return true;
        }
    }
    return false;
}

void WMain::killThread( unsigned idx )
{
    if ( idx == THREAD_KILL_ALL )
    {
        for( unsigned cnt=0; cnt<MAX_THREADS_CNT; cnt++ )
        {
            killThread( cnt );
        }

        return;
    }
    else
    if ( ( idx >= 0 ) && ( idx < MAX_THREADS_CNT ) )
    {
        if ( threads[idx] != NULL )
        {
            pthread_t ptt = threads[idx]->ptt;
            if ( ptt > 0 )
            {
                pthread_kill( ptt, 0 );
                pthread_join( ptt, NULL );
            }

            delete threads[idx];
            threads[idx] = NULL;
        }
    }
}
